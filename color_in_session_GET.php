<?php
define('GET_COLOR', 'color');
define('SESS_COLOR', 'color');
session_start();

if (array_key_exists(GET_COLOR, $_GET)) {
    $_SESSION[SESS_COLOR] = $_GET[GET_COLOR];
}
if (array_key_exists(SESS_COLOR, $_SESSION)) {
    $color = $_SESSION[SESS_COLOR];
} else {
    $color = "";
}
?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Color in session with GET command</title>
    <style>
        body {
            background-color: <?= $color ?>;
        }
    </style>
</head>
<body>
    <header>
        <h1>Color in session with GET command</h1>
        <nav>
            <a href="<?= $_SERVER['PHP_SELF'] ?>">Retour au get</a>
            <a href="<?= $_SERVER['PHP_SELF'] ?>?<?= GET_COLOR ?>=yellow">Yellow</a>
            <a href="<?= $_SERVER['PHP_SELF'] ?>?<?= GET_COLOR ?>=pink">Pink</a>
            <a href="<?= $_SERVER['PHP_SELF'] ?>?<?= GET_COLOR ?>=orange">Orange</a>
        </nav>
    </header>
    <main>
    </main>
</body>

</html>