<?php
define('CHECKED_ATTR', 'checked="checked"');

$city = array();
if (array_key_exists('city', $_POST)) {
    $city = $_POST['city'];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>City choice form</title>
</head>

<body>
    <header>
        <h1>City choice form</h1>
        <nav>
            <a href="<?= $_SERVER['PHP_SELF'] ?>">Back to GET mode</a>
        </nav>
    </header>
    <main>
        <form method="post">
            <div>
                <label for="montreal">Montréal</label>
                <input type="checkbox" name="city[]" id="montreal" value="montreal" <?= in_array("montreal", $city) ? CHECKED_ATTR : "" ?> />
            </div>
            <div>
                <label for="quebec">Québec</label>
                <input type="checkbox" name="city[]" id="quebec" value="quebec" <?= in_array("quebec", $city) ? CHECKED_ATTR : "" ?> />
            </div>
            <div>
                <label for="sherbrooke">Sherbrooke</label>
                <input type="checkbox" name="city[]" id="sherbrooke" value="sherbrooke" <?= in_array("sherbrooke", $city) ? CHECKED_ATTR : "" ?> />
            </div>
            <div>
                <input type="submit" value="Submit" name="submit" />
            </div>
        </form>
    </main>
</body>

</html>